package demo4.globalCommand.services;

import java.util.List;

import demo4.globalCommand.entity.Item;

public interface ItemService {
	 List<Item> listAll();
	Item addItem(Item item);

}
