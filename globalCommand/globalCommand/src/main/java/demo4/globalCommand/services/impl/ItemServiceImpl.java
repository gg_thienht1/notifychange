package demo4.globalCommand.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import demo4.globalCommand.entity.Item;
import demo4.globalCommand.services.ItemService;

@Service("itemService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)

public class ItemServiceImpl implements ItemService {
	@Autowired
	ItemDao itemDao;

	public List<Item> listAll() {
		// TODO Auto-generated method stub
		return itemDao.listItemDao();
	}

	public Item addItem(Item item) {
		// TODO Auto-generated method stub
		return itemDao.addItem(item);
	}

}
