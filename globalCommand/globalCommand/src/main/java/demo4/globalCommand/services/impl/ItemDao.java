package demo4.globalCommand.services.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import demo4.globalCommand.entity.Item;

@Repository
public class ItemDao {
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<Item> listItemDao(){
		return em.createQuery("SELECT i FROM Item i", Item.class).getResultList();
	}
	@Transactional
	public Item addItem(Item item) {
		em.persist(item);
		em.flush();
		return item;
	}
}
