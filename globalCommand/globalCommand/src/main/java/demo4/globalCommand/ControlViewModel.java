package demo4.globalCommand;

import org.zkoss.zk.ui.select.annotation.VariableResolver;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ControlViewModel {

	private boolean visible;
	
	public void show() {
		visible = true;
	}
	
	public void hide() {
		visible = false;
	}
	
}
