package demo4.globalCommand;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import demo4.globalCommand.entity.Item;
import demo4.globalCommand.services.ItemService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ListViewModel {
	@WireVariable
	private ItemService itemService;
	
	private List<Item> listItem;
	
	private Date lastUpdate;
	@Init
	public void init() {
		
		List<Item> itemList = itemService.listAll();
		listItem = new ArrayList<Item>(itemList);
	}
	@GlobalCommand @NotifyChange({"listItem","lastUpdate"})
	public void refresh() {
		listItem = new ArrayList<Item>();
	}

	public List<Item> getListItem() {
		List<Item> itemList = itemService.listAll();
		listItem = new ArrayList<Item>(itemList);
		return listItem;
	}

	public void setListItem(List<Item> listItem) {
		this.listItem = listItem;
	}

	public Date getLastUpdate() {
		lastUpdate = Calendar.getInstance().getTime();
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	


}
