package demo4.globalCommand;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import demo4.globalCommand.entity.Item;
import demo4.globalCommand.services.ItemService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class AddViewModel {
 
	@WireVariable
	private ItemService itemService;
	
	private Item item;
	private String msg;
	
	@Init
	public void init() {
		item = new Item();
	}
	
	@Command @NotifyChange("msg")
	public void addItem() {
		itemService.addItem(item);
		msg = "add "+item.getName();
	}

	public Item getItem() {
		return item;
	}
	@Command
	public void resetItem() {
		item = new Item();
		
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
