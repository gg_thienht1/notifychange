package demo3.demoNotifyChange.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import demo3.demoNotifyChange.entity.Category;
import demo3.demoNotifyChange.services.CategoryService;

@Service("categoryService")
@Scope(value="singleton",proxyMode= ScopedProxyMode.TARGET_CLASS)

public class CategoryServiceImpl implements CategoryService{

	@Autowired
	CategoryDao categoryDao;
	
	public List<Category> getCategories() {
		// TODO Auto-generated method stub
		return categoryDao.findAllCategory();
	}

}
