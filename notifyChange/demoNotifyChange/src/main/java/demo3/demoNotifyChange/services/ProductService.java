package demo3.demoNotifyChange.services;

import java.util.List;

import demo3.demoNotifyChange.entity.Product;

public interface ProductService {
	List<Product> getProducts();
	Product add(Product product);
	
}
