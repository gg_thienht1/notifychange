package demo3.demoNotifyChange.services.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import demo3.demoNotifyChange.entity.Category;

@Repository
public class CategoryDao {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<Category> findAllCategory(){
		return em.createQuery("SELECT c FROM Category c", Category.class).getResultList();
	}
}
