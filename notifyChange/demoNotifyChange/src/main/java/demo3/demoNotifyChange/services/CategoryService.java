package demo3.demoNotifyChange.services;

import java.util.List;

import demo3.demoNotifyChange.entity.Category;

public interface CategoryService {
	List<Category> getCategories();
}
