package demo3.demoNotifyChange.services;

import demo3.demoNotifyChange.entity.Log;
import java.util.List;

public interface MyService {

	Log addLog(Log log);

	List<Log> getLogs();

	void deleteLog(Log log);
}
