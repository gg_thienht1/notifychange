package demo3.demoNotifyChange.services.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import demo3.demoNotifyChange.entity.Product;

@Repository
public class ProductDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<Product> findAllProduct(){
		return em.createQuery("SELECT p FROM Product p", Product.class).getResultList();
	}

	@Transactional
	public Product save(Product product) {
		Product p = new Product();
		p.setId(p.getId());
		p.setName(p.getName());
		p.setPrice(p.getPrice());
		if(product.getId() != 0) {
			em.merge(product);
		}
		else {
		em.persist(product);}
		em.flush();
		return product;
	}
}
