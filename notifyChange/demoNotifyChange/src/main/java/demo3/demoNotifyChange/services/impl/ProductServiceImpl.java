package demo3.demoNotifyChange.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import demo3.demoNotifyChange.entity.Product;
import demo3.demoNotifyChange.services.ProductService;

@Service("productService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;
	public List<Product> getProducts() {
		// TODO Auto-generated method stub
		return productDao.findAllProduct();
	}
	public Product add(Product product) {
		// TODO Auto-generated method stub
		return productDao.save(product);
	}

}
