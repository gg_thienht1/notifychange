package demo3.demoNotifyChange;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import demo3.demoNotifyChange.entity.Category;
import demo3.demoNotifyChange.entity.Product;
import demo3.demoNotifyChange.services.CategoryService;
import demo3.demoNotifyChange.services.ProductService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ProductViewModel {

	@WireVariable
	private ProductService productService;
	@WireVariable
	private CategoryService categoryService;
	private Product product = new Product();
	private List<Product> productList;
	private List<Category> categoryList;
	
	@Init
	public void init() {
		List<Product> products = productService.getProducts();
		productList = new ArrayList<Product>(products);
		List<Category> category = categoryService.getCategories();
		categoryList = new ArrayList<Category>(category);
	}
	public void edit(@BindingParam("product") Product p) {
		product=p;
		productService.add(product);
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public List<Product> getProductList() {
		List<Product> products = productService.getProducts();
		productList = new ArrayList<Product>(products);
		return productList;
	}
	@GlobalCommand
	public void reset() {
		getProductList();
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}
	
}
