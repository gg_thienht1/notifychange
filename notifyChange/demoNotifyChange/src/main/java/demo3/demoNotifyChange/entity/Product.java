package demo3.demoNotifyChange.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import demo3.demoNotifyChange.services.ProductService;

@Entity
@Table(name="product")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class Product {
	@WireVariable
	@Transient
	ProductService productService;
	
	@Id
	@GeneratedValue
	@Column(name="productId")
	private int id;
	
	@Column(name="productName")
	private String name;
	
	@Column(name="price")
	private double price;

	
	public Product() {
		super();
	}

	public Product(int id, String name, double price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void addProduct(@BindingParam("product") Product product, @BindingParam("p") Object p) {
		productService.add(product);
		BindUtils.postNotifyChange(null, null, p, "productList");

	}
	

}
