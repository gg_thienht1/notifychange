package demo3.demoNotifyChange.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="category")
public class Category {
	@Id
	@GeneratedValue
	@Column(name="categoryId")
	private int id;
	
	@Column(name="nameCategory")
	private String nameCategory;

	public Category() {
		super();
	}

	public Category(int id, String nameCategory) {
		super();
		this.id = id;
		this.nameCategory = nameCategory;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameCategory() {
		return nameCategory;
	}

	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}


}
